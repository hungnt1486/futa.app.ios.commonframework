// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "FutaCommonFramework",
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "FutaCommonFramework",
            targets: ["FutaCommon"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        
//        .binaryTarget(
//                    name: "FutaCommon",
//                    url: "https://rms.atsoftreward.com/FutaCommon.xcframework.zip",
//                    checksum: "1afd8992f3314e5de3d1f7411d59e339fd2fcae2e007f9a54dc957fce2171b41"),
        .binaryTarget(name: "FutaCommon", path: "./Sources/FutaCommon.xcframework")
//        .binaryTarget(
//            name: "FutaCommon",
//            url: "https://gitlab.com/hungnt1486/futa.test/-/raw/97e67416877aec2b5376fd991b990dc8a59c6782/Sources/FutaTest/FutaCommon.xcframework.zip",
//            checksum: "1afd8992f3314e5de3d1f7411d59e339fd2fcae2e007f9a54dc957fce2171b41"
//        )
        
//        .target(
//            name: "FutaCommonFramework",
//            dependencies: []),
        
    ]
)
